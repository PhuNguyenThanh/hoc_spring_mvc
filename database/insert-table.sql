use hc_spring_mvc;

insert into roles(code,name) values('ADMIN','Quản trị');
insert into roles(code,name) values('USER','Người dùng');

insert into users(userName,passwords,full_name,status)
values('admin','$2a$10$/RUbuT9KIqk6f8enaTQiLOXzhnUkiwEJRdtzdrMXXwU7dgnLKTCYG','trương tùng lâm',1);
insert into users(userName,passwords,full_name,status)
values('nguyenvanausers','$2a$10$/RUbuT9KIqk6f8enaTQiLOXzhnUkiwEJRdtzdrMXXwU7dgnLKTCYG','nguyễn văn A',1);
insert into users(userName,passwords,full_name,status)
values('nguyenvanb','$2a$10$/RUbuT9KIqk6f8enaTQiLOXzhnUkiwEJRdtzdrMXXwU7dgnLKTCYG','nguyễn văn B',1);

INSERT INTO user_roles(userid,roleid) VALUES (1,1);
INSERT INTO user_roles(userid,roleid) VALUES (2,2);
INSERT INTO user_roles(userid,roleid) VALUES (3,2);