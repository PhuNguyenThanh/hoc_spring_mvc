package com.hcSpringMVC.security;

import com.hcSpringMVC.util.SecurityUtils;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

// nơi sử lý authortication sau khi authentication
// khai báo annotation @Component để giúp Spring biết nó là một Bean class
@Component
public class CustomSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {
	
	private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();
	
	@Override
	public void handle(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
				throws IOException {
		// targetUrl là khai báo để định tuyến url nào sẽ trả về theo từng role
		String targetUrl = determineTargetUrl(authentication);
		if (response.isCommitted()) {
			return;
		}
		// gửi tới controller cần tới
		redirectStrategy.sendRedirect(request, response, targetUrl);
	}
	
	public RedirectStrategy getRedirectStrategy() {
		return redirectStrategy;
	}

	public void setRedirectStrategy(RedirectStrategy redirectStrategy) {
		this.redirectStrategy = redirectStrategy;
	}

	private String determineTargetUrl(Authentication authentication) {
		String url = "";
		// để có thể lấy đc role của user ta tạo 1  class trong package utils có tên SecurityUtils
		List<String> roles = SecurityUtils.getAuthorities();
		// nếu role là admin thì ta sẽ redirect tới trang admin
		if (isAdmin(roles)) {
			url = "/admin/home";
		} else if (isUser(roles)) // nếu role là user thì ta sẽ redirect tới trang trang chủ
			{
			url = "/home";
		}
		return url;
	}

	private boolean isAdmin(List<String> roles) {
		// kiểm tra có list roles có chứa role admin ko
		if (roles.contains("ADMIN")) {
			return true;
		}
		return false;
	}

	private boolean isUser(List<String> roles) {
		// kiểm tra có list roles có chứa role user ko
		if (roles.contains("USER")) {
			return true;
		}
		return false;
	}
}
