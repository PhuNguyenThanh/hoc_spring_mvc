package com.hcSpringMVC.DTO;



public class CommentDTO extends AbstracDTO<CommentDTO> {

	private String content;
	private long userid;
	private long newsId;
	
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public long getUserid() {
		return userid;
	}
	public void setUserid(long userid) {
		this.userid = userid;
	}
	public long getNewsId() {
		return newsId;
	}
	public void setNewsId(long newsId) {
		this.newsId = newsId;
	}


	// tạo nhanh genarate get-set bằng cách Alt+Shift+S




}
