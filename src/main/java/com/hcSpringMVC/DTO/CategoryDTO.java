package com.hcSpringMVC.DTO;



public class CategoryDTO extends AbstracDTO<CategoryDTO> {

	private String name;
	private String code;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}



	// tạo nhanh genarate get-set bằng cách Alt+Shift+S
	



}
