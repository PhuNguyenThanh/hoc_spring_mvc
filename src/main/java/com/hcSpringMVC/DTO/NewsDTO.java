package com.hcSpringMVC.DTO;



public class NewsDTO extends AbstracDTO<NewsDTO> {

	private String title;
	private String thumbnail;
	private String shortDescription;
	private String content;
	private long categoryId;
	// mảng danh sách bài viết admin xóa
	private long[] ids ;
	//


	// tạo nhanh genarate get-set bằng cách Alt+Shift+S
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getThumbnail() {
		return thumbnail;
	}
	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}
	public String getShortDescription() {
		return shortDescription;
	}
	public long[] getIds() {
		return ids;
	}
	public void setIds(long[] ids) {
		this.ids = ids;
	}
	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public long getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(long categoryId) {
		this.categoryId = categoryId;
	}



}
