package com.hcSpringMVC.DTO;

import java.util.ArrayList;
import java.util.List;

public class AbstracDTO<T> {
	private long id;
	private String createdDate; // Timestamp của java.sql
	private String modifiedDate;
	private String createdBy;
	private String modifiedBy;
	/*
	 * khi từ controller trả về view thì luôn luôn trả về dạng là list chỉ khác các
	 * class Model nên khai báo một list để khi trong AbstracModel để các model có
	 * thể lấy được các list
	 * 
	 */
	// lưu ý ko nên viết hoa chữ cái đầu tên biến
	private List<T> listResults = new ArrayList<>();
	// các biến dùng trong phân trang
	private Integer page;// trang hiện tại
	private Integer maxPageItem;// số lượng item xuất hiện trên 1 trang
	// cách tính total page = total Item/ Max page Item
	private Integer totalPage;// total page: tổng trang phân chia
	private Integer totalItem;// tổng số item sẽ được hiển thị
	// các biến dùng trong sort
	private String sortName;
	private String sortBy;

	public String getSortName() {
		return sortName;
	}

	public void setSortName(String sortName) {
		this.sortName = sortName;
	}

	public String getSortBy() {
		return sortBy;
	}

	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}

	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

	public Integer getMaxPageItem() {
		return maxPageItem;
	}

	public void setMaxPageItem(Integer maxlPageItem) {
		this.maxPageItem = maxlPageItem;
	}

	public Integer getTotalPage() {
		return totalPage;
	}

	public void setTotalPage(Integer totalPage) {
		this.totalPage = totalPage;
	}

	public Integer getTotalItem() {
		return totalItem;
	}

	public void setTotalItem(Integer totalItem) {
		this.totalItem = totalItem;
	}

	public List<T> getListResults() {
		return listResults;
	}

	public void setListResults(List<T> ListResults) {
		listResults = ListResults;
	}

	// tạo nhanh genarate get-set bằng cách Alt+Shift+S
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

}
