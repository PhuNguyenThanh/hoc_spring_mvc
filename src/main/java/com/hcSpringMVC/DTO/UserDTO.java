package com.hcSpringMVC.DTO;

public class UserDTO extends AbstracDTO<UserDTO> {

	private String userName;
	private String fullName;
	private String password;
	private int status;
	private long roldId;
	

	// tạo nhanh genarate get-set bằng cách Alt+Shift+S

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public long getRoldId() {
		return roldId;
	}

	public void setRoldId(long roldId) {
		this.roldId = roldId;
	}


}
