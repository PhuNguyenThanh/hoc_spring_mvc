package com.hcSpringMVC.constant;

public class SystemConstant {
	// lưu trữ các biến cố định không bao giờ thay đổi trong hệ thống
	public static final String MODEL="model";
	public static final int ACTIVE_STATUS=1;
	public static final int INACTIVE_STATUS=0;
}
