package com.hcSpringMVC.util;

import java.util.ArrayList;
import java.util.List;

import com.hcSpringMVC.DTO.MyUser;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;



public class SecurityUtils {
	
	public static MyUser getPrincipal() {
		// hàm lấy Principal(chứa thông tin của user đăng nhập đã lưu vào spring security)
		MyUser myUser = (MyUser) (SecurityContextHolder.getContext()).getAuthentication().getPrincipal();
        return myUser;
    }
	
	
	@SuppressWarnings("unchecked")
	public static List<String> getAuthorities() {
		List<String> results = new ArrayList<>();
		// ở tầng service chúng ta đã đưa thông tin role của user vào GrantedAuthority spring security
		// nên ở đây chúng ta sẽ lấy role từ GrantedAuthority đã lưu
		List<GrantedAuthority> authorities = (List<GrantedAuthority>)(SecurityContextHolder.getContext().getAuthentication().getAuthorities());
        for (GrantedAuthority authority : authorities) {
            results.add(authority.getAuthority());
        }
		return results;
	}
}
