package com.hcSpringMVC.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.hcSpringMVC.DTO.CategoryDTO;

public class categoryMapper implements IRowmapper<CategoryDTO>{
// lấy data từ 1 row ResultSet truyền vào class model rồi trả về 
	@Override
	public CategoryDTO mapRow(ResultSet resultSet) {
	
		try {	
		CategoryDTO category = new CategoryDTO();
		category.setId(resultSet.getLong("id"));
		category.setName(resultSet.getString("name"));
		category.setCode(resultSet.getString("code"));
		return category;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			return null;
		}
		
		
	}

}
