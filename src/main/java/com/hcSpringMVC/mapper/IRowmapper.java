package com.hcSpringMVC.mapper;

import java.sql.ResultSet;

public interface IRowmapper<T> {
	// dùng Generic để tham số háo các class model
	// lấy data từ 1 row ResultSet truyền vào class model rồi trả về 
	// vì đây là một hoạt dộng liên tục lặp lại khi query dữ liệu đến các table nên làm thành 1 pakage riêng
	T mapRow(ResultSet resultSet);
}
