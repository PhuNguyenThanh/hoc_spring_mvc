package com.hcSpringMVC.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.hcSpringMVC.DTO.NewsDTO;

public class newMapper implements IRowmapper<NewsDTO>{
	// lấy data từ 1 row ResultSet truyền vào class model rồi trả về 
	// phải lấy đúng tên trong table dữ liệu
	// 
	@Override
	public NewsDTO mapRow(ResultSet resultSet) {
		//resultSet.get+kiểu_dữ_liệu("tên column trong data base")
		try {	NewsDTO news1 = new NewsDTO();
			news1.setId(resultSet.getLong("id"));
			news1.setTitle(resultSet.getString("title"));
			news1.setContent(resultSet.getString("content"));
			news1.setCategoryId(resultSet.getLong("categoryId"));
			news1.setThumbnail(resultSet.getString("thumball"));
			news1.setShortDescription(resultSet.getString("sortdescription"));
			news1.setCreatedBy(resultSet.getString("createdby"));
			news1.setCreatedDate(resultSet.getString("createddate"));
			
			// khi update mà ko có thông tin về ngày chỉnh sửa và người chỉnh sửa thì không in ra
			if(resultSet.getString("modifiedby") != null ) {
				news1.setModifiedBy(resultSet.getString("modifiedby"));
			}	
			if(resultSet.getString("modifieddate") != null) {
				news1.setModifiedDate(resultSet.getString("modifieddate"));	
			}
			return news1;
		} catch (SQLException e) {
			return null;
		}
		
	}

}
