package com.hcSpringMVC.repository;

import com.hcSpringMVC.entity.userEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<userEntity, Long> {
	userEntity findOneByUserNameAndStatus(String username, int status);
}
