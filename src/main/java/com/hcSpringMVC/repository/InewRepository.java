package com.hcSpringMVC.repository;

import com.hcSpringMVC.entity.newEntity;
import org.springframework.data.jpa.repository.JpaRepository;
// để springframework hiểu class này là 1 repository(DAO)thì ta khai báo annotation @Repository
// có thể không khai báo annotation @Repository khi in Interface đã extends JpaRepository
// vì JpaRepository đã đc hiểu là repository

// JpaRepository<newEntity, Long>:JpaRepository<tên entity, kiểu khóa chính của bảng>
public interface InewRepository extends JpaRepository<newEntity, Long> {
    // vì JpaRepository là 1 api mà interface nó thừa kế lại nên nó sử dụng các hàm đã tạo sẵn nên
    // không có class nếu như câu sql quá phức tạp thì:...
}
