package com.hcSpringMVC.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
// muốn dùng security.core.Authentication phải thêm thư viện spring security ở pom
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;


import java.util.Collection;

@Configuration// khai báo annotation để biết đây là 1 config của hệ thống
@EnableJpaAuditing(auditorAwareRef = "auditorProvider")
// khai báo annotation để có thể sử dụng JpaAuditing
public class JpaAuditingConfig {

    @Bean
    public AuditorAware<String> auditorProvider() {
        return new AuditorAwareImpl();
    }

    public static class AuditorAwareImpl implements AuditorAware<String> {
        // config để JpaAuditing có thể set username ra từ spring security
        @Override
        public String getCurrentAuditor() {
            // khai báo Authentication để lấy thông tin từ spring security
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            // nếu authentication = null khi user chưa dăng nhập hoặc rỗng
            if (authentication == null) {
                return null;
            }
            Collection authorities;
            return authentication.getName();
        }
    }
}
