package com.hcSpringMVC.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.Properties;

@Configuration
// khai báo annotation để biết đây là 1 config của hệ thống
@EnableTransactionManagement // khai báo annotation để có thể sử dụng entity transaction
// entity transaction có thể tự động mở và đóng connection database, tự đông commit and rollback

// annotation @EnableJpaRepositories khai báo và bật JpaRepositories nơi chứa repository(data access
// layer)
@EnableJpaRepositories(basePackages = {"com.hcSpringMVC.repository"})
public class JPAConfig {
    // dùng để khởi tạo load entityManagerFactory để tạo các entity Manager để thực hiện câu sql
    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        // dataSource là nơi cung cấp các thông số để connection với database
        em.setDataSource(dataSource());
        // đọc nơi ở của các entity ở trong file persistence trong META-INF của resource
        em.setPersistenceUnitName("persistence-data"); // còn đc gọi là chất súc tắc
        JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);
        // đọc các tính chất
        em.setJpaProperties(additionalProperties());
        return em;
    }
    // khai báo để load các thông số của  connection
    @Bean
    public DataSource dataSource() { // dataSource của javax.sql
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        // bắt buộc cài drive mysql maven ở pom.xml
        dataSource.setDriverClassName("com.mysql.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql://localhost:3306/hc_spring_mvc");
        // cấu trúc url = "jdbc:mysql://hostname:port/dbname";
        dataSource.setUsername("root");
        dataSource.setPassword("123456");
        return dataSource;
    }
    // khai báo để set các tính chất của jpa
    Properties additionalProperties() { // Properties của java.util
        Properties properties = new Properties();
        // "create-drop" giúp khởi tạo và drop khi stop serve database từ entity lúc mới bắt đầu dự
        // án
        // properties.setProperty("hibernate.hbm2ddl.auto", "create-drop");
        // lúc dự án đã ổn định thì không cần phải chỉnh sửa table từ entity -> "none"
         properties.setProperty("hibernate.hbm2ddl.auto", "none");
        // "create" giúp khởi tạo và không drop khi stop serve database từ entity lúc mới bắt đầu dự
        // án tuy nhiên sẽ xóa tất cả data trong database -> cần insert data
        //properties.setProperty("hibernate.hbm2ddl.auto", "create");
        // config fetchType lazy
        properties.setProperty("hibernate.enable_lazy_load_no_trans", "true");
        return properties;
    }
    // khởi tạo entity transaction manager
    @Bean
    JpaTransactionManager transactionManager(EntityManagerFactory entityManagerFactory) {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory);
        return transactionManager;
    }

    @Bean
    public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
        return new PersistenceExceptionTranslationPostProcessor();
    }
}
