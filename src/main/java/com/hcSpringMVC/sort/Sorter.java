package com.hcSpringMVC.sort;

public class Sorter {
// sử lý sắp xếp khi gọi OderBy sql
	private String sortName;
	private String sortBy;
	public Sorter(String SortName,String SortBy) {
		this.sortBy=SortBy;
		this.sortName=SortName;
	}
	public String getSortName() {
		return sortName;
	}
	public void setSortName(String sortName) {
		this.sortName = sortName;
	}
	public String getSortBy() {
		return sortBy;
	}
	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}
}
