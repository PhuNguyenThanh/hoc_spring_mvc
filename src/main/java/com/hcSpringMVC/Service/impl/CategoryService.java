package com.hcSpringMVC.Service.impl;


import com.hcSpringMVC.Service.ICategoryService;
import com.hcSpringMVC.DTO.CategoryDTO;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryService implements ICategoryService {
    // nhúng DAO vào Service @Inject sẽ tự tìm hàm ở service tránh việc phải new nhiều nhần
    // contrutor
   // @Autowired private ICategoryDAO categoryDAO;
    /* nếu ko dùng @Inject
    private ICategoryDAO categoryDAO;
    public CategoryService() {
    	categoryDAO =new CategoryDAO();
    	categoryDAO.findAll();
    }*/
    @Override
    public List<CategoryDTO> findAll() {
        return null;
    }
}
