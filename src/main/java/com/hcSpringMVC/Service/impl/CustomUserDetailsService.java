package com.hcSpringMVC.Service.impl;

import com.hcSpringMVC.DTO.MyUser;
import com.hcSpringMVC.constant.SystemConstant;
import com.hcSpringMVC.entity.roleEntity;
import com.hcSpringMVC.entity.userEntity;
import com.hcSpringMVC.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CustomUserDetailsService implements UserDetailsService {
    @Autowired private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        userEntity user = userRepository.findOneByUserNameAndStatus(username, SystemConstant.ACTIVE_STATUS);
        if (user == null) {
            // nếu không tìm thấy user name -> user_entity=null sẽ ném ra lỗi
            // UsernameNotFoundException để authentication-failure-url có thể hứng đc failed đó
            throw new UsernameNotFoundException("username not found");
        }
        // lấy code của role nhúng vào authorities
        List<GrantedAuthority> authorities = new ArrayList<>();
        for (roleEntity role : user.getRoles()) {
            authorities.add(new SimpleGrantedAuthority(role.getCode()));
        }
        // nếu user != null
        // put thông tin user vào spring security để duy trì thông tin đăng nhập của user khi login
        // vào hệ thống
        // vì trong spring security chỉ cho phép lưu username và password nên ta tạo MyUser trong DTO kế thừa User của spring security
        // để có thể lưu thêm các thông tin khác như fullName...
        MyUser userdetails =
                new MyUser(
                        user.getUserName(),
                        user.getPassword(),
                        true,
                        true,
                        true,
                        true,
                        authorities);
        userdetails.setFullName(user.getFullName());
        return userdetails;
    }
}
