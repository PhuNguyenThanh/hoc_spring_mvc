package com.hcSpringMVC.Service;

import com.hcSpringMVC.DTO.CategoryDTO;

import java.util.List;

public interface ICategoryService {

    List<CategoryDTO> findAll();
}
