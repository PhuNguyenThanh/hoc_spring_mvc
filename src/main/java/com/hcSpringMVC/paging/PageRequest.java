package com.hcSpringMVC.paging;

import com.hcSpringMVC.sort.Sorter;

public class PageRequest implements Pageble {
	// sử lý lấy offset,page,maxPageItem, sortName,sortBy để phân trang và sắp xếp 
	// dùng để truyền tham số đến service và DAO thay thì phải truyền- tham số thì chỉ cần truyền 1 object 
	// giảm thiểu số tham sô phải truyền
	private Integer page;
	private Integer maxPageItem;
	private Sorter sorter;
	
	public PageRequest(Integer Page,Integer MaxPageItem , Sorter Sorter) {
		this.page=Page;
		this.maxPageItem=MaxPageItem;
		this.setSorter(Sorter);
	}
	@Override
	public Integer getPage() {
		return this.page;
	}

	@Override
	public Integer getOffset() {
		if(this.page!= null || this.maxPageItem!= null) {
			return (this.page - 1)* this.maxPageItem;
		}
		return null;
	}

	@Override
	public Integer getLimit() {
		return this.maxPageItem;
	}
	@Override
	public Sorter getSorter() {
		return sorter;
	}
	public void setSorter(Sorter sorter) {
		this.sorter = sorter;
	}

}
