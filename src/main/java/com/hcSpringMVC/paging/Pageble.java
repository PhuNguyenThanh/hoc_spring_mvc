package com.hcSpringMVC.paging;

import com.hcSpringMVC.sort.Sorter;

public interface Pageble {
	// được tạo để giảm thiểu số lượng tham số cần phải truyền giữa các tầng: controler-service-DAO
	Integer getPage();
	Integer getOffset();
	Integer getLimit();
	Sorter getSorter();
}
