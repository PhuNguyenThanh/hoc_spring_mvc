package com.hcSpringMVC.controller.web;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

// ở trước các class controller thêm @Controller để xác định class đó
// là 1 controller theo mô hình M-V-C spring
// nếu trong cùng project để tên của 2 controller giống nhau :VD: homeController
// thì cần có value ="" để phân biệt hai controller
@Controller(value = "homeControllerOfWeb")
public class homeController {

    // @RequestMapping là nơi nhận các url request
    // khi người dùng request 1 url thì hệ thống sẽ tìm tất cả class có @Controller
    // rồi tìm xuống các @RequestMapping xem các value có cái nào trùng với url mà
    // người dùng request
    // sau đó sẽ trả về 1 trang jsp trong views
    // method = RequestMethod.GET sử dụng để lấy thông tin từ database lên view thì
    // thường sẽ là RequestMethod.GET
    // method = RequestMethod.DELETE sử dụng để xóa thông tin trong database thì
    // thường sẽ là RequestMethod.DELETE
    // method = RequestMethod.POST sử dụng để insert thông tin trong database thì
    // thường sẽ là RequestMethod.POST
    // method = RequestMethod.PUT sử dụng để cập nhập thông tin trong database thì
    // thường sẽ là RequestMethod.PUT
    @RequestMapping(value = "/home", method = RequestMethod.GET)
    public ModelAndView homePage() {
        // không cần phải khai báo đuôi file .jsp
        // và đường dẫn đến thư mục views (được cấu hình trong bean
        // dispatcher-servlet.xm
        // trong beans của file dispatcher-servlet.xml
        ModelAndView mav = new ModelAndView("web/home");
        return mav;
    }

    @RequestMapping(value = "/dang-nhap", method = RequestMethod.GET)
    public ModelAndView dangnhap() {
        ModelAndView mav = new ModelAndView("login");
        return mav;
    }

    @RequestMapping(value = "/thoat", method = RequestMethod.GET)
    public ModelAndView logout(HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
            // hàm sử lý remove session trong spring security
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        // chuyển về home của user
        return new ModelAndView("redirect:/home");
    }

    @RequestMapping(value = "/accessDenied", method = RequestMethod.GET)
    public ModelAndView accessDenied() {
        return new ModelAndView("redirect:/dang-nhap?accessDenied");
    }
}
