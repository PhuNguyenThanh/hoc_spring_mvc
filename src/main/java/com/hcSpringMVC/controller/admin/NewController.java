package com.hcSpringMVC.controller.admin;

import com.hcSpringMVC.Service.InewServive;
import com.hcSpringMVC.DTO.CommentDTO;
import com.hcSpringMVC.DTO.NewsDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller(value = "newControllerOfAdmin")
public class NewController {

    // để sài @autowired thì cần cấu hình beans trong dispatcher-servlet.xml
    @Autowired private InewServive newServive;

    @RequestMapping(value = "/admin/new/list", method = RequestMethod.GET)
    // @ModelAttribute Annotation ModelAttribute là một cách bổ sung, nó giúp bind
    // tham số hoặc kết quả trả về của một phương thức(url hoặc từ 1 controller khác truyền qua)
    // truyền qua thành một model attribute dưới tên được chỉ định là model.
    // NewsModel model khai báo model sử dụng trong controller đó
    public ModelAndView showList(
                @ModelAttribute("model") NewsDTO model, CommentDTO modelComment) {
        // ModelAndView Là sự kết hợp của 2 khía cạnh truyền dữ liệu và view.
        // Như ta thấy ở ví dụ trên ta dùng 2 dòng code.
        // mav.addObject("model", model); gán dữ liệu model cho biến model.
        // new ModelAndView("admin/new/list") ; trả về trang view là list.jsp.
        // Chúng ta sử return mav để thực hiện việc ,trả về trang list.jsp và mode 1 lần

        // chuyển trang điều hướng đến list.jsp
        ModelAndView mav = new ModelAndView("admin/new/list");
        // thực thi functinon của findAll và gán nó vào model
        model.setListResults(newServive.findAll());
        model.getCategoryId();
        // thêm 1 biến object vào ModelAndView
        mav.addObject("model", model);
        // trả về view có chứa dữ liệu model
        return mav;
    }

    @RequestMapping(value = "/new-list-edit", method = RequestMethod.GET)
    public ModelAndView editNew(@ModelAttribute("model") NewsDTO model) {
        ModelAndView mav = new ModelAndView("admin/new/edit");
        return mav;
    }
}
