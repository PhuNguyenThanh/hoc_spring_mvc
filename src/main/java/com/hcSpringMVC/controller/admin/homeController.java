package com.hcSpringMVC.controller.admin;

import com.hcSpringMVC.Service.ICategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
// ở trước các class controller thêm @Controller để xác định class đó
// là 1 controller(presentation layer) theo mô hình 3 tier web architure

// nếu trong cùng project để tên của 2 controller giống nhau :VD: homeController
// thì cần có value ="" để phân biệt hai controller
@Controller(value = "homeControllerOfAdmin")
public class homeController {

    @Autowired private ICategoryService categoryService;

    // @RequestMapping là nơi nhận các url request
    // khi người dùng request 1 url thì hệ thống sẽ tìm tất cả class có @Controller
    // rồi tìm xuống các @RequestMapping xem các value có cái nào trùng với url mà người dùng
    // request sau đó sẽ trả về 1 trang jsp trong views
    // method = RequestMethod.GET sử dụng để lấy thông tin từ database lên view thì
    // thường sẽ là RequestMethod.GET
    // method = RequestMethod.DELETE sử dụng để xóa thông tin trong database thì
    // thường sẽ là RequestMethod.DELETE
    // method = RequestMethod.POST sử dụng để insert thông tin trong database thì
    // thường sẽ là RequestMethod.POST
    // method = RequestMethod.PUT sử dụng để cập nhập thông tin trong database thì
    // thường sẽ là RequestMethod.PUT

    @RequestMapping(value = "/admin/home", method = RequestMethod.GET)
    public ModelAndView homePage() {
        // không cần phải khai báo đuôi file .jsp
        // và khai báo đường dẫn đến thư mục views
        // (được cấu hình trong bean dispatcher-servlet.xm)
        ModelAndView mav = new ModelAndView("admin/home");
        return mav;
    }
}
