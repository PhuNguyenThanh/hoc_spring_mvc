package com.hcSpringMVC.entity;

import javax.persistence.*;
import javax.persistence.JoinColumn;
import java.util.ArrayList;
import java.util.List;

@Entity // khai báo annotation để biết class này là 1 entity
@Table(name = "users") // khai báo annotation để biết đây là 1 table tên news
public class userEntity extends baseEntity {
    // khai báo annotation để biết đó là 1 column tên title
    @Column(name = "username")
    private String userName;

    @Column(name = "passwords")
    private String password;
    // columnDefinition = "TEXT NOT NULL" để định dạng kiểu cho table và cho phép null
    @Column(name = "full_name", columnDefinition = "TEXT NULL")
    private String fullName;
    // nullable = true để định dạng cho phép Column có được null hay không
    @Column(name = "status", nullable = true)
    private Integer status;

    // khai báo annotation để biết mối quan hệ giữa hai table là 1-n hay n-1 hay 1-1
    @ManyToMany(fetch = FetchType.LAZY)
    // khai báo annotation để biết quan hệ với bảng nào
    @JoinTable(
            name = "user_roles",
            // name tên table cần tạo quan hệ (vì đây là quan hệ n-n nên sẽ tạo ra 1 bảng mới để
            // quan hệ)
                joinColumns = @JoinColumn(name = "userid"),
            // joinColumns đầu tiên cần phải để tên column của bảng mà đoạn code này đứng
            inverseJoinColumns = @JoinColumn(name = "roleid"))
    // inversejoinColumns cần phải để tên column của bảng cần quan hệ
    private List<roleEntity> roles = new ArrayList<>();

    // vì đây quan hệ n-n nên có 2 cái @joinColumn nếu là 1-n hay n-1 hoặc 1-1 thì chỉ cần 1
    // VD: @ManyToOne
    //     @JoinColumn(name = "role_id") // thông qua khóa ngoại role_id
    //     @EqualsAndHashCode.Exclude // không sử dụng trường này trong equals và hashcode
    //     @ToString.Exclude // Khoonhg sử dụng trong toString()
    //     private roleEntity role;
    // VD: @OneToOne
    //     @JoinColumn(name = "role_id")
    //     private roleEntity role;
    // VD: @OneToMany(mappedBy = "role_id", cascade = CascadeType.ALL) // Quan hệ 1-n với đối tượng
    // ở
    //    dưới (role) (1 user có nhiều người role)
    //    MapopedBy trỏ tới tên biến role_id ở trong roles.
    //    Cascade là cơ chế cho phép lan truyền quá trình chuyển đổi từ một entity đến các related
    // entity(các entity có liên quan)
    //    @EqualsAndHashCode.Exclude // không sử dụng trường này trong equals và hashcode
    //    @ToString.Exclude // Khoonhg sử dụng trong toString()
    //    private List<roleEntity> role;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
    
    public List<roleEntity> getRoles() {
        return roles;
    }
    
    public void setRoles(List<roleEntity> roles) {
        this.roles = roles;
    }
}
