package com.hcSpringMVC.entity;

import javax.persistence.*;

@Entity // khai báo annotation để biết class này là 1 entity
@Table(name = "roles") // khai báo annotation để biết đây là 1 table tên news
public class roleEntity extends baseEntity {
  // khai báo annotation để biết đó là 1 column tên title
  @Column(name = "name")
  private String name;

  @Column(name = "code")
  private String code;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }
}
