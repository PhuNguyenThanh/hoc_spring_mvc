package com.hcSpringMVC.entity;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;
// khi extends baseEntity thì sẽ không thể ánh xạ các column(tạo-thao tác) có trong baseEntity
// nên cần khai báo annotation để biết đây là entity cha của các entity con thừa kế
@MappedSuperclass
// khai báo annotation để sử dụng JPA auditing giúp thêm createdate, modifydate createby, modifyby
// một cách tự động mà không cần phải sử lý ở tầng service
// trước khi sử dụng cần config JpaAuditing ở JpaAuditingConfig ở package config
@EntityListeners(AuditingEntityListener.class)
// nơi khai báo các column luôn có trong các table nên chỉ cần viết trong abstract class baseEntity
// mà không cần viết trong các entity thừa kế
public abstract class baseEntity {
    // vì id nó là khóa chính không thể null và tự tăng nên
    @Id // xác định đó là là khóa chính và ko thể null
    @GeneratedValue(strategy = GenerationType.IDENTITY) // để giá trị tự động tăng
    private long id;

    @Column(name = "createdate", columnDefinition = "DATE NUll")
    // khai báo annotation để JPA auditing set dữ liệu createDate 1 cách tự động
    @CreatedDate
    private Date createDate;

    @Column(name = "modifieddate", columnDefinition = "DATE NUll")
    // khai báo annotation để JPA auditing set dữ liệu modifiedDate 1 cách tự động
    @LastModifiedDate
    private Date modifiedDate;

    @Column(name = "createdby", columnDefinition = "TEXT null")
    // khai báo annotation để JPA auditing set dữ liệu createdby 1 cách tự động
    @CreatedBy
    private String createdby;

    @Column(name = "modifiedby", columnDefinition = "TEXT null")
    // khai báo annotation để JPA auditing set dữ liệu modifiedBy 1 cách tự động
    @LastModifiedBy
    private String modifiedBy;

    public long getId() {
        return id;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public String getCreatedby() {
        return createdby;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }
}
