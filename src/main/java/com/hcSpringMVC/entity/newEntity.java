package com.hcSpringMVC.entity;

import javax.persistence.*;

@Entity // khai báo annotation để biết class này là 1 entity
@Table(name = "news") // khai báo annotation để biết đây là 1 table tên news
public class newEntity extends baseEntity {

  // khai báo annotation để biết đó là 1 column tên title
  @Column(name = "title")
  private String title;

  @Column(name = "thumbnail")
  private String thumbnail;
  // columnDefinition = "TEXT" để định dạng kiểu cho table
  @Column(name = "shortdescription", columnDefinition = "TEXT")
  private String shortDescription;

  @Column(name = "content", columnDefinition = "TEXT")
  private String content;

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getThumbnail() {
    return thumbnail;
  }

  public void setThumbnail(String thumbnail) {
    this.thumbnail = thumbnail;
  }

  public String getShortDescription() {
    return shortDescription;
  }

  public void setShortDescription(String shortDescription) {
    this.shortDescription = shortDescription;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }
}
