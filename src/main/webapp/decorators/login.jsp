<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ include file="/common/taglib.jsp" %>
<!-- PUBLIC"-//W3C//DTD -->
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title> <dec:title default="Đăng Nhập" /> </title>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link rel="stylesheet"
          href="<c:url value='/template/LOGIN/login.css' />" />
</head>
<body>

<div class="container">
    <!-- chỗ thay đổi khi load page -->
    <dec:body></dec:body>
</div>




</body>
</html>