<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@ include file="/common/taglib.jsp" %>
<!-- PUBLIC"-//W3C//DTD -->
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title> <dec:title default="Trang chu" /> </title>
<!-- css -->

    <link href="<c:url value='/template/web/bootstrap/css/bootstrap.min.css' />" rel="stylesheet" type="text/css" media="all"/>
    <link href="<c:url value='/template/web/css/style.css' />" rel="estyleshet" type="text/css" media="all"/>
</head>
<body>
	<!-- header -->
	<!-- chỗ ko thay đổi khi load page -->
		<%@ include file="/common/web/header.jsp" %>
	<!-- header -->
	
	<div class="container">
	<!-- chỗ thay đổi khi load page -->
		<dec:body></dec:body>
	</div>
	<!-- footer -->
	<!-- chỗ ko thay đổi khi load page -->
	<%@ include file="/common/web/footer.jsp" %>
	<!-- footer -->
	
	<!-- jquery -->
<script type="text/javascript" src="<c:url value='/template/web/jquery/jquery.min.js'></c:url>" ></script>
<script type="text/javascript" src="<c:url value='/template/web/bootstrap/js/bootstrap.bundle.min.js'></c:url>" ></script>

</body>
</html>