<!-- khai báo các thư viện sẽ sử dụng trong jsp -->
<!-- thư viện jstl  -->
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!-- thư viện master layout  -->
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="dec"%>
<!-- thư viện copy -->
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
