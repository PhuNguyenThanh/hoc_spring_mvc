<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.hcSpringMVC.util.SecurityUtils"%>
<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <a class="navbar-brand" href="#">Start Bootstrap</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
              <a class="nav-link" href="#">Trang chủ
                <span class="sr-only">(current)</span>
              </a>
            </li>
            <!-- kiểm tra xem người dùng đã đăng nhập chưa nếu chưa:isAnonymous()-> thì hiện nút đăng nhập nếu đã có:isAuthenticated() thì hiện nút thoát -->
            <security:authorize access="isAuthenticated()" >
              <c:if test="${not empty USERMODEL}">
                <li class="nav-item">
                  <a class="nav-link" href='#'>Wellcome <%SecurityUtils.getPrincipal().getFullName(); %> </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href='<c:url value="/thoat"/>'>Thoát</a>
                </li>
              </c:if>
            </security:authorize>
            <security:authorize access="isAnonymous()" >
              <c:if test="${empty USERMODEL}">
                <li class="nav-item">
                  <a class="nav-link" href='<c:url value="/dang-nhap"/>'>Đăng nhập</a>
                </li>
              </c:if>
            </security:authorize>

          </ul>
        </div>
      </div>
</nav>